Graças a APIs fornecidas por empresas como o Google e Microsoft, podem ser incluídos recursos interessantes de geolocalização em nossos sistemas.
Além de funcionalidades que vão desde exibir mapa com pontos coloridos  de endereços conhecidos, podemos oferecer recursos como roteamento, distância de deslocamento e até custo estimado de pedágio e transporte público.

Este aplicativo foi o que uso para teste da API e novas funcionalidades e, daí, repasso a minha aplicação principal.

Estou disponibilizando a comunidade e, espero ser útil.